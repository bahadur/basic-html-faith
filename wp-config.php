<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'fooseshoes');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'asdf123');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/** Changing default admin url*/

define('WP_ADMIN_DIR', 'shoes-manager');
define( 'ADMIN_COOKIE_PATH', SITECOOKIEPATH . WP_ADMIN_DIR); 

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '8g3S21*Ey^Uob+A0`PW][nMJLmY#k1w ]]k-[3[Wm#C/w8N;tlOuks1Yhm`ocbF9');
define('SECURE_AUTH_KEY',  '#jd4fEMYtc;*YaJv}/x-llrDSsgA4:>,XIEvKJL6,qSG#P>]kFs%7aQU|u!=$THO');
define('LOGGED_IN_KEY',    '5)m~KgN;gD({q_M;+U#&yq`xlz7Ou:O/HL)]mKq4f +?dtkKo[ OK13wcq8<D+ng');
define('NONCE_KEY',        ':_qqNPGhXNmfFgN?}JM`{4;mN:]v0(f} TqyjeJX*?s7A,[l|X7O,Aj69i4Yy80q');
define('AUTH_SALT',        'jcv4O(Sg{9X.OTKPRy@]c36hiQj9|n%_&s@uRt1o#0T44gl+[oP{}2bfT.8=`>jk');
define('SECURE_AUTH_SALT', 'qdgC.3rx 7<#iy9|mU%ybGdEq7GwZy~QfKx>}BWY@K%,Iz%X5+/*t@x@K4d!t;v-');
define('LOGGED_IN_SALT',   ':vc>t[E-zBRZv~e~3z!z*i5tZT?X|LgK?NBK=n/Qv.AsBHE$3;zKC{/s/?;)?dQ?');
define('NONCE_SALT',       'K7+w{5k}W>.,-ycQk5$U!S{0n3H;!XphIR!-m0Z|NT8&DI,ky x0</Z:6wUt4$iY');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
