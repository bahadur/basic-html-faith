<?php get_header();?>

<section class="product-list">
	<div class="container ">
		
		<div class="row list-filter">
		
			<div class="col-md-6">
				<h3>New arrivals on FooseShoes</h3>
			</div>

			<div class="col-md-6">
				<button class="btn btn-detault  pull-right">Show All</button>
			</div>
		
		</div>
			
		<div class="row list-group">
			<?php echo do_shortcode('[recent_products per_page="3" columns="3"]');?>
		</div>

		<div class="row list-filter">
			
			<div class="col-md-6">
				<h3>Best sellers of the month</h3>
			</div>

			<div class="col-md-6">
				<button class="btn btn-detault  pull-right">Show All</button>
			</div>

		</div>

		<div class="row list-group">
			<?php echo do_shortcode('[best_selling_products per_page="3" columns="3"]');?>
		</div>
			
	</div>
</section>




<?php get_footer();?>