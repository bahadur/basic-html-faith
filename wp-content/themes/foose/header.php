<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width">
		<title><?php bloginfo('name'); ?></title>
		<?php wp_head(); ?>
	</head>

	<body <?php body_class(); ?>>

		<header>
			<nav class="navbar navbar-inverse" id="navbar-first">
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				            <span class="sr-only">Toggle navigation</span>
				            <span class="icon-bar"></span>
				            <span class="icon-bar"></span>
				            <span class="icon-bar"></span>
				          </button>
						<a class="navbar-brand" href="<?php echo home_url(); ?>">FOOSESHOES</a>
					</div>
					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					 	<form class="navbar-form navbar-right search" role="search">
					        <div class="input-group">
						        <input type="search" class="form-control">
						        <div class="input-group-btn">
						            <button type="submit" class="btn btn-default">
						                <span class="glyphicon glyphicon-search"></span>
						            </button>
						        </div>
						    </div>
						    <button>LOGIN or REGISTER</button>
					      </form>
				     </div>
				    
				</div>

				

			</nav>


			<nav class="navbar navbar-default" id="navbar-second">
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle cooapsed" data-toggle="collapse" data-target="#collapse-navbar-second">
							
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
					</div>

					<div class="collapse  navbar-collapse" id="collapse-navbar-second">
						<!--ul class="nav navbar-nav">
							<li class="active"><a href="index.html">Home</a></li>
							<li><a href="products.html">Products</a></li>
							<li><a href="#">About</a></li>
							<li><a href="#">Pages</a></li>
							<li><a href="#">Blog</a></li>
							<li><a href="#">Contact</a></li>
						</ul-->
						<?php 
						$args = array(
							'theme_location' => __('primary'),
							'items_wrap' => '<ul class="nav navbar-nav">%3$s</ul>'


						);

						wp_nav_menu( $args );
						?>
						<ul class="nav navbar-nav pull-right">
							<!--li><a href="#">
									<i class="fa fa-star-o">
										<span class="label label-default"><?php //echo "0" ?></span>
									</i>
								</a>
							</li-->
							<li><a href="<?php echo get_site_url()?>/cart">
									<i class="fa fa-opencart">
										<span class="label label-green"><?php echo WC()->cart->get_cart_contents_count(); ?></span>
									</i>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</nav>
		</header>

		<?php if ( is_front_page() ):  ?>
		<section class="section-showcase-home hidden-xs">
			<div class="container showcase-content">
				<?php $args = 
				array(
				    'posts_per_page'   => 1,
				    'orderby'          => 'rand',
				    'post_type'        => 'product' 
			    ); 

				$random_products = get_posts( $args );

				foreach ( $random_products as $post ) : setup_postdata( $post ); 
				$product = new WC_Product( get_the_ID() );
				?>
			    
				<div class="row">
					<div class="col-md-6 col-sm-4"></div>
					<div class="col-md-6 col-sm-8">
						
						<div class="price-box">
							<span><?php echo $product->get_price_html(); ?></span>	
						</div>
						<h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?><br> <?php echo date('Y') ?> Collection</a></h1>
						<p class="lead"><?php the_content() ?></p>
						<i class="fa fa-eye border-square"></i>
						<i class="fa fa-star-o border-square"></i>
						<i class="fa fa-mail-forward border-square"></i>
						<i class="fa fa-shopping-cart border-square-green"></i>

					</div>
				</div>			
				<?php endforeach; 
				wp_reset_postdata(); ?>
			</div>
		</section>

		<section class="section-category hidden-xs">
			<div class="cat-top">
				<div class="container">
					<div class="row">
						<div class="col-sm-2 col-md-2">
							<div class="top-category-bar">
								<div class="bar-selected"></div>

								<div class="category-selected">
									<h4>Pink Shoes</h4>
									<p class="small">Now off $145.99</p>
								</div>				
							
							</div>
						</div>

						<div class="col-sm-2 col-md-2">
							<div class="top-category-bar">
								<div class="bar"></div>

								<div class="category">
									<h4>Anna Field</h4>
									<p class="small">Limited Edition</p>
								</div>				
							</div>
							
						</div>

						<div class="col-sm-2 col-md-2">
							<div class="top-category-bar">
								<div class="bar"></div>

								<div class="category">
									<h4>Prada</h4>
									<p class="small">Summer is coming</p>
								</div>				
							</div>
							
						</div>

						<div class="col-sm-2 col-md-2">
							<div class="top-category-bar">
								<div class="bar"></div>

								<div class="category">
									<h4>Casadei</h4>
									<p class="small">All Colors available</p>
								</div>				
							</div>
							
						</div>

						<div class="col-sm-2 col-md-2">
							<div class="top-category-bar">
								<div class="bar"></div>

								<div class="category">
									<h4>Mellow Yellow</h4>
									<p class="small">Free delivery</p>
								</div>				
							</div>
							
						</div>

						<div class="col-sm-2 col-md-2">
							<div class="top-category-bar">
								<div class="bar"></div>

								<div class="category">
									<h4>Pink Shoes</h4>
									<p class="small">Now off $145.99</p>
								</div>				
							</div>
							
						</div>


					</div>
					
				</div>
			</div>
			<div class="container">
				<div class="row">
					<div class="col-md-4 col-sm-4 col-xs-4 special">
						

						<img src="<?php echo get_template_directory_uri(); ?>/img/on_sale.jpg" />

						<div class="special-text">ON SALE</div>

					</div>
					<div class="col-md-4 col-sm-4 col-xs-4 special">
						
						<img src="<?php echo get_template_directory_uri(); ?>/img/must_have.jpg" />
						<div class="special-text">SPECIAL OFFER</div>

					</div>
					<div class="col-md-4 col-sm-4 col-xs-4 special">
						
						<img src="<?php echo get_template_directory_uri(); ?>/img/special_offer.jpg" />
						<div class="special-text">MUST HAVE</div>

					</div>
				</div>
			</div>

		</section>
		<?php else: ?>
		<section class="section-showcase-product  hidden-xs">
		    <div class="container showcase-content">
		      
		      <div class="row">
		        <div class="col-md-6 col-sm-4"></div>
		        <div class="col-md-6 col-sm-8">
		          <?php 

		          	$args = 
						array(
						    'posts_per_page'   => 1,
						    'orderby'          => 'rand',
						    'post_type'        => 'product' 
					    ); 



		          	$random_products = get_posts( $args );

					foreach ( $random_products as $post ) : setup_postdata( $post ); 
					$product = new WC_Product( get_the_ID() );
					?>
		          <div class="price-box">
		            <span><?php echo $product->get_price_html(); ?></span>  
		          </div>
		          <h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?><br> <?php echo date('Y') ?> Collection</a></h1>
					<p class="lead"><?php the_content() ?></p>
				<?php endforeach; 
				wp_reset_postdata(); ?>

		        </div>
		      </div>      
		      
		    </div>
		</section>
	<?php endif; ?>

		<div class="container">