<?php 

/*
	==========================
		admin url
	==========================
*/


// add_filter('site_url',  'wpadmin_filter', 10, 3);
// function wpadmin_filter( $url, $path, $orig_scheme ) {  
//     $old  = array( "/(wp-admin)/");  
//     $admin_dir = WP_ADMIN_DIR;  
//     $new  = array($admin_dir);  
//     return preg_replace( $old, $new, $url, 1);  
// }

add_action('after_setup_theme', 'woocommerce_support');

function woocommerce_support(){
	add_theme_support('woocommerce');
}


function feeseWP_resources(){

	wp_enqueue_style('style', get_stylesheet_uri());
	wp_enqueue_style('bootstrap', get_template_directory_uri().'/css/bootstrap.css');
	wp_enqueue_style('font-awesome', get_template_directory_uri().'/css/font-awesome.css');
	wp_enqueue_style('main', get_template_directory_uri().'/css/main.css');
	wp_enqueue_script('jquery',get_template_directory_uri().'/js/jquery.js');
	wp_enqueue_script('bootstrap',get_template_directory_uri().'/js/bootstrap.js');
}

add_action('wp_enqueue_scripts','feeseWP_resources');



register_nav_menus( array(
	'primary' => __( 'Primary Menu',      'foose' ),
	'footer'  => __( 'Footer Menu', 'foose' ),
) );






remove_action('woocommerce_before_shop_loop_item_title','woocommerce_template_loop_product_thumbnail',10);
add_action('woocommerce_before_shop_loop_item_title', 'custom_woocommerce_template_loop_product_thumbnail',10);


function custom_woocommerce_template_loop_product_thumbnail(){
	global $post;
	$image_size = apply_filters( 'single_product_archive_thumbnail_size', $size );

	if ( has_post_thumbnail() ) {
		$props = wc_get_product_attachment_props( get_post_thumbnail_id(), $post );
		echo get_the_post_thumbnail( 
			$post->ID, $image_size, array(
				'title'	 => $props['title'],
				'alt'    => $props['alt'],
			) 
		);

	} elseif ( wc_placeholder_img_src() ) {
		
		echo wc_placeholder_img( $image_size );

	}

}




remove_action('woocommerce_shop_loop_item_title','woocommerce_template_loop_product_title',10);
add_action('woocommerce_shop_loop_item_title', 'custom_woocommerce_template_loop_product_title',10);


function custom_woocommerce_template_loop_product_title() {
		
		echo '<div class="caption">
            <h4 class="group inner list-group-item-heading">
            	' . get_the_title() . '
             </h4>
            </div>';

		//echo '<h3>' . get_the_title() . '</h3>';
	}



remove_action('woocommerce_after_shop_loop_item_title','woocommerce_template_loop_price',10);
add_action('woocommerce_after_shop_loop_item_title', 'custom_woocommerce_template_loop_price',10);

function custom_woocommerce_template_loop_price() {
		wc_get_template( 'loop/price.php' );
	}



remove_action('woocommerce_before_shop_loop_item','woocommerce_template_loop_product_title',10);
add_action('woocommerce_before_shop_loop_item', 'custom_woocommerce_template_loop_product_link_open',10);

function custom_woocommerce_template_loop_product_link_open() {
	//echo '<a href="' . get_the_permalink() . '" class="woocommerce-LoopProduct-link">';
	echo '<div class="thumbnail">';

}




remove_action('woocommerce_after_shop_loop_item','woocommerce_template_loop_product_link_close',10);
add_action('woocommerce_after_shop_loop_item', 'custom_woocommerce_template_loop_product_link_close',10);
function custom_woocommerce_template_loop_product_link_close() {
	echo '</div>';
}

//remove_action('woocommerce_before_main_content','woocommerce_output_content_wrapper',10);
add_action('woocommerce_archive_description','custom_woocommerce_output_content_wrapper',20);

function custom_woocommerce_output_content_wrapper(){
	echo '<section class="product-list-shop"><div class="container ">';

}

add_action('woocommerce_after_shop_loop','custom_woocommerce_output_content_wrapper_end',20);

function custom_woocommerce_output_content_wrapper_end(){
	echo '</div></section>';

}



add_filter('loop_shop_columns','loop_columns',999);

function loog_columns(){
	return 4;
}

//add_filter('loop_shop_per_page', create_function('$cols', 'return 2;'),20);

function home_page_menu_args( $args ) {
$args['show_home'] = true;
return $args;
}
add_filter( 'wp_page_menu_args', 'home_page_menu_args' );


/*
	==========================
		Sidebar functions
	==========================
*/

function foose_widget_setup(){

	register_sidebar(
		array(
			'name' 	=> 'Sidebar',
			'id' 	=> 'sidebar-1',
			'class'	=> 'custom',
			'description' => 'Standard Sidebar',
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'	=> '</aside>',
			'before_title'	=> '<h2 class="widget-title">',
			'after_title'	=> '</h2>'
		)
	);

	register_sidebar(
		array(
			'name' 	=> 'Sidebar Product',
			'id' 	=> 'product-sidebar-1',
			'class'	=> 'product-custom',
			'description' => 'Product Sidebar',
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'	=> '</aside>',
			'before_title'	=> '<h2 class="widget-title">',
			'after_title'	=> '</h2>'
		)
	);

	
}

add_action('widgets_init','foose_widget_setup');


/*
	==========================
		checkout
	==========================
*/

add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );

// Our hooked in function - $fields is passed via the filter!
function custom_override_checkout_fields( $fields ) {
     

      //$fields['billing']['billing_first_name']['class'] = array('form-control');
     // $fields['billing']['billing_first_name']['label'] = 'My new label';
     return $fields;
}
?>