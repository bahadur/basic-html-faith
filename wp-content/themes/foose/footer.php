		</div>
		
		<section class="section-misc">
			<div class="container">
			<hr>
				<div class="row">
					<div class="col-md-3">
						<div class="block-gray">
							<h4>FREE SHIPPING</h4>
							<p>Vivamus metus turpis, bibendum vitae euismod vel, vulputate vel nibh. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec erat sem, </p>
						</div>
					</div>

					<div class="col-md-3">
						<div class="block-gray">
							<h4>TESTIMONIALS</h4>
							<p>vel, vulputate vel nibh. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec erat sem, vehicula id dictum sit [...]
	 						<br> - <i>John Doe</i> -</p>
						</div>
					</div>
					<div class="col-md-6">
						<div class="block-gray">
							<h4>BLOG NEWS</h4>
							<div class="media">
								<div class="media-left">
									<a href="#">
										<div class="date-box">
											<span>APR<br>01</span>
										</div>
									</a>
								</div>
								<div class="media-body">
									<h4>Nice & clean. The best for you blog!</h4>
									<p>Vivamus metus turpis, bibendum vitae euismod vel, vulputate vel </p>
								</div>
							</div>
							<div class="media">
								<div class="media-left">
									<a href="#">
										<div class="date-box">
											<span>APR<br>01</span>
										</div>
									</a>
								</div>
								<div class="media-body">
									<h4>What an Ecommerce theme.</h4>
									<p>Vivamus metus turpis, bibendum vitae euismod vel</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section class="section-dark">
			<div class="container">
				<div class="row">
					<div class="col-md-4">
						<div class="block-dark">
							<h4>TEXT WIDGET</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
						</div>
					</div>

					<div class="col-md-4">
						<div class="block-dark">
							<h4>TWITTER WIDGET</h4>
							<p>
								<span class="text-primary">@ericafustero</span> Why thank you. Your work looks awesome by the way! <span class="text-primary">@treemelody</span>
								<p>19 days ago</p>
							</p>
							<p>
								<span class="text-primary">@roymarvelous</span>You can seek a refund through TF if it is not as advertised - but it is :)
								<p>21 days ago</p>
							</p>
						</div>
					</div>

					<div class="col-md-4">
						<div class="block-dark">
							<h4>FLICKER WIDGET</h4>
							<div class="row list-group">
								
								<div class="col-md-4 col-sm-4 thumb">
									
										<img class="group list-group-image" src="<?php echo get_template_directory_uri(); ?>/img/flkr1.png">
									
								</div>
								<div class="col-md-4 col-sm-4 thumb">
									
										<img class="group list-group-image" src="<?php echo get_template_directory_uri(); ?>/img/flkr2.png">
									
								</div>
								<div class="col-md-4 col-sm-4 thumb">
									
										<img class="group list-group-image" src="<?php echo get_template_directory_uri(); ?>/img/flkr3.png">
									
								</div>
								<div class="col-md-4 col-sm-4 thumb">
									
										<img class="group list-group-image" src="<?php echo get_template_directory_uri(); ?>/img/flkr4.png">
									
								</div>
								<div class="col-md-4 col-sm-4 thumb">
									
										<img class="group list-group-image" src="<?php echo get_template_directory_uri(); ?>/img/flkr5.png">
									
								</div>
								<div class="col-md-4 col-sm-4 thumb">
									
										<img class="group list-group-image" src="<?php echo get_template_directory_uri(); ?>/img/flkr6.png">
									
								</div>
							
							</div>
							
						</div>
					</div>
				</div>
			</div>
			<hr class="hr-dark">
			<div class="container container-second">
				<div class="row">
					<div class="col-md-6">
						<form class="navbar-form navbar-left search" role="search">
					        <div class="input-group">
						        <input type="search" class="form-control" placeholder="Enter email for newsletter">
						        <div class="input-group-btn">
						            <button type="submit" class="btn btn-default">
						                <span class="fa fa-chevron-right"></span>	
						            </button>
						        </div>
						    </div>
						    
					      </form>
					</div>

					<div class="col-md-6">
						
							
							<i class="fa fa-flickr fa-2x border-square pull-right"></i>
							<i class="fa fa-google-plus fa-2x border-square pull-right"></i>
							<i class="fa fa-twitter fa-2x border-square pull-right"></i>
							<i class="fa fa-facebook fa-2x border-square pull-right"></i>

						
					</div>
				</div>

			</div>
		</section>

		<footer>
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<p>Copyright Fooseshoes 2016.<br>
						Designed by Bello. Powered by Wordpress.
						</p>
					</div>
					<div class="col-md-6">
						
							<!--ul>
								<li><a href="">Home</a></li>
								<li><a href="">Support</a></li>
								<li><a href="">Terms and Conditions</a></li>
								<li><a href="">Faqs</a></li>
								<li><a href="">Contact us</a></li>

							</ul-->
							<?php 
							$args = array(
								'theme_location' => __('footer')
							);

						wp_nav_menu( $args );
						?>
						
					</div>
				</div>
			</div>
		</footer>

		<?php wp_footer(); ?>
	</body>
</html>