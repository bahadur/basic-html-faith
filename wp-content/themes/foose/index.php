<?php 

get_header();
// $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
// $args = array('posts_per_page' => 2, 'paged' => $paged );
// query_posts($args);
?>
	<section class="section-posts">
		<div class="container">
			
			<div class="row">
				<div class="col-md-8">
					<?php 
					
					if(have_posts()) :
						while (have_posts()) : the_post(); 
					?>
					<article class="post">
						<h2><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
						<small><?php the_time('F jS, Y')  ?> by <?php the_author_posts_link()?></small>
						<p>
						

						
						<?php 
						if ( is_single() || is_page() ):
							if(has_post_thumbnail()): 
								the_post_thumbnail('medium'); 
							endif; 
							the_content();

						else:

							if(has_post_thumbnail()): 
								the_post_thumbnail('medium'); 
							else : 
								the_excerpt();
							
							endif;
						endif;
						?>
						</p>
						
					</article>
					<?php endwhile; ?>
					<!-- pagination -->
				<?php next_posts_link(); ?>
				<?php previous_posts_link(); ?>

				<!-- No posts found -->

				<?php else :
					echo '<p>No content found</p>';
				endif;
					?>
				</div>

				<div class="col-md-4">
					<?php
					if ( ! is_woocommerce() ) { get_sidebar(); }
					 ?>	
				</div>
			</div>

			
		</div>
	</section>

	
<?php 
get_footer();
?>