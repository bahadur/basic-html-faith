<?php 

get_header();
// $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
// $args = array('posts_per_page' => 2, 'paged' => $paged );
// query_posts($args);
?>
	<section class="section-posts">
		<div class="container">
			<?php 
					
			if(have_posts()) :
				while (have_posts()) : the_post(); 
					the_content();
				endwhile; 
			else :
				echo '<p>No content found</p>';
			endif;
			?>
			
		</div>
	</section>

	
<?php 
get_footer();
?>